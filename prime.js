function isSimpleDigit(numb) {
    if (numb <= 1) {
        return false;
    }

    for (var i = 2; i < numb / 2 + 1; i++) {
        if (numb % i === 0) {
            return false;
        }
    }
    return true;
}

function prime(start, end) {
    for (var k = start; k <= end; k++) {
        console.log('Digit \'' + k + '\' is ' + (isSimpleDigit(k) === true ? 'prime!' : 'not prime.'));
    }
}

prime(-20, 20);